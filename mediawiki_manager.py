#!/usr/bin/env python2.7

'''
This module handles MediaWiki Pages
'''
import argparse, sys, ConfigParser, logging, os
from  utils.constants import LOG_FILE, LOG_FORMAT, SECTION_GENERAL, SECTION_WIKI, \
                TEXT_CONTENT, FILE_PATH, WIKI_PAGE, WIKI_API, WIKI_USERNAME, WIKI_PASSWORD, \
                ACTION, ACTION_COLLECT, ACTION_INJECT, OUTPUT_DIR
import pdfkit
from wikitools import wiki
from wikitools import page
from wikitools import wikifile
from wikitools import api


class MediaWikiManager(object):
    '''
    This class handles MediaWiki Pages
    '''

    wiki_api = None
    wiki_username = None
    wiki_password = None
    site = None

    def __init__(self, initparams):
        logging.basicConfig(
            filename=initparams[LOG_FILE],
            level=logging.DEBUG,
            format=LOG_FORMAT)
        self.wiki_api = initparams[WIKI_API]
        self.wiki_username = initparams[WIKI_USERNAME]
        self.wiki_password = initparams[WIKI_PASSWORD]
         # create a Wiki object
        self.site = wiki.Wiki(self.wiki_api) 
        # login - required for read-restricted wikis
        self.site.login(self.wiki_username, self.wiki_password)

    def upload_file(self, file_path):
        '''
        Updload a file into MediaWiki
        Returns wiki file name
        '''
        #logging.info(u"Uploading file[{0}]:" \
        #           .format(file_path))
        filename = os.path.basename(file_path)
        filename = filename.replace(':', "_").replace('<', '_').replace('>', '_')
        if not isinstance(filename, unicode):
            filename = unicode(filename, "utf-8")
        filename = filename.encode('ascii', 'ignore')
        wiki_file = wikifile.File(self.site, filename, check=True)
        file_o = open(file_path, 'rb')
        res = wiki_file.upload(file_o, ignorewarnings=True)
        file_o.close()
        return "Media:"+filename


    def update_page(self, page_title, content, backup_path = None):
        '''
        Updates a MediaWiki Page
        with the provided content (substitutes existing if any)
        and attachement
        '''
        logging.info(u'Updating page [{0}]: \
                    \ntext = [{1}]'.format(page_title, content))
        
        wikipage = page.Page(self.site, page_title)
        if(backup_path is not None):
            with open(backup_path, 'w') as outfile:
                try:
                    outfile.write(wikipage.getWikiText())
                except page.NoPage:
                    logging.error("Empty page, nothing to backup")

        # TODO: Fix this terrible hack to avoid the API refusing to post due to UTF8 chars
        wikipage.edit(text=content.encode('ascii', 'ignore'))

    def store_page_as_pdf(self, page_title, output_dir, authorship_info=""):
        '''
        Downloads the contents of a given page
        and stores it as pdf into the provided destination path
        '''
        #TODO: Find out why the following line crashes
        #logging.info(u'Storing page [{0}]'.format(page_title))
        wikipage = page.Page(self.site, page_title)
        # define the params for the query
        params = {'action': 'parse',
                 'text' : wikipage.getWikiText()}
        # create the request object
        request = api.APIRequest(self.site, params)
        # query the API
        queryresult = request.query()
        htmlcontent = queryresult['parse']['text']['*']
        filename = page_title.replace(" ", "")+".pdf"
        if authorship_info is not "":
            authorship_info += "-"
        output_path = os.path.join(output_dir, authorship_info+filename)
        options = {
            'encoding': "UTF-8"
        }
        pdfkit.from_string(htmlcontent, output_path, options=options)
        return output_path



def parse_config_file(inifile):
    '''
    Parse .ini configuration file at inifile
    '''
    result = {}
    try:
        cfgparser = ConfigParser.ConfigParser()
        cfgparser.read(inifile)
        result = {
            LOG_FILE:
            cfgparser.get(SECTION_GENERAL, LOG_FILE),

            OUTPUT_DIR:
            cfgparser.get(SECTION_GENERAL, OUTPUT_DIR),

            WIKI_API:
            cfgparser.get(SECTION_WIKI, WIKI_API),

            WIKI_USERNAME:
            cfgparser.get(SECTION_WIKI, WIKI_USERNAME),

            WIKI_PASSWORD:
            cfgparser.get(SECTION_WIKI, WIKI_PASSWORD),

            WIKI_PAGE:
            cfgparser.get(SECTION_WIKI, WIKI_PAGE),

            TEXT_CONTENT:
            cfgparser.get(SECTION_WIKI, TEXT_CONTENT),

            FILE_PATH:
            cfgparser.get(SECTION_WIKI, FILE_PATH),
            }
    except Exception:
        raise IOError("Unable to read [{0}].\
            Check that the configuration file exists and has a valid format"
            .format(inifile))
    return result

def replace_defaults_with_arguments(defaults, arguments):
    '''
    Taking the dictionary defaults as reference,
    replace (or add if necessary) any value for the matching keys in arguments
    '''    
    params = defaults
    for param in arguments:
        if arguments[param] is not None:
            params[param] = arguments[param]

    return params


def parse_arguments():
    '''
    This function parses command line parameters
    and load all the necessary information from a config file
    '''

    parser = argparse.ArgumentParser( description = \
        "Create or update MediaWiki pages.\
        Handles content as hypertext, but also files.")

    parser.add_argument('-f', '--function', action = "store",
        dest = ACTION,
        help = "Action to perform: available actions are {0} and {1}"
                .format(ACTION_COLLECT, ACTION_INJECT))
    parser.add_argument('-p', '--page', action = "store",
        dest = WIKI_PAGE,
        help = "Wiki page to create, update or retrieve (depends on the requested action)")
    parser.add_argument('-t', '--text', action = "store",
        dest = TEXT_CONTENT,
        help = "Path to the text file containing the new content of the page")
    parser.add_argument('-a', '--attach', action = "store",
        dest = FILE_PATH,
        help = "Path to the file to attach")
    parser.add_argument('-c', '--configfile', action = "store",
        dest="inifile",
        help = "Configuration parameters provided inside a .ini file. \
        These parameters will be overwritten by inline parameters")

    args  = parser.parse_args(sys.argv[1:])
    
    cfgparams = {}
    if args.inifile is not None:
        # Load the defaults in the .ini file
        cfgparams = parse_config_file(args.inifile)

    # Replace defaults with the parameters provided in the command line
    parameters = replace_defaults_with_arguments(cfgparams, vars(args))

    return parameters

if __name__ == '__main__':
    ARGS = parse_arguments()

    # Instantiate the gdoc manager with the provided credentials
    
    MANAGER = MediaWikiManager(ARGS)
    
    if ARGS[ACTION] == ACTION_INJECT:
        WIKI_FILE_NAME = MANAGER.upload_file(
            file_path = ARGS[FILE_PATH]
        )

        WIKI_TEXT = ARGS[TEXT_CONTENT] + u"  [[{0}|Link to test file]]".format(WIKI_FILE_NAME)
        MANAGER.update_page(
            page_title = ARGS[WIKI_PAGE],
            content = WIKI_TEXT
        )
    elif ARGS['action'] == ACTION_COLLECT:
        FILEPATH = MANAGER.store_page_as_pdf(ARGS[WIKI_PAGE], ARGS[OUTPUT_DIR])
        print("Check the results at {0}".format(FILEPATH))
    else:
        print("Unable to complete action [{0}]. Available actions are {1} and {2}".
            format(ARGS[ACTION], ACTION_COLLECT, ACTION_INJECT))

    print("== DONE! ==")
