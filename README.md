# Media Wiki utils #

> ### Disclaimer: WIP ###
>
> The code in this repo is continous **work in progress** to learn about python and the API to manage mediawiki pages.
> Feel free to use some of it, but take always the **master** branch as a reference.
>
> ***Leave your questions and feedback as new [issues](https://bitbucket.org/alozalv/mediawiki_utils/issues).***
>
> *** Add this repo as a [git submodule](https://bitbucket.org/alozalv/mediawiki_utils/wiki/How%20to%20add%20this%20repo%20as%20a%20git%20submodule) in your project ***
>
> Note that [wikitools](https://pypi.python.org/pypi/wikitools) and [poster](https://pypi.python.org/pypi/poster) (required by the former) need to be installed in your system.
>
> Thanks!

This repository contains a number of modules to handle Media Wiki pages.

Inside the *mediawiki_utils* folder, you'll find:

+ *mediawiki_manager.py*: MediaWikiManager class. Made runnable for examplary purposes (see below).
+ *config*: configuration folder. Copy the sample and replace the values on it with your own.
+ *utils*: some utility modules such as constants.

You can run *mediawiki_manager.py* in order to upload new content into the wiki (function *inject*) or download a wiki page as PDF (function *collect*).


```
>> ./mediawiki_manager.py -h                                                                                     
usage: mediawiki_manager.py [-h] [-f ACTION] [-p WIKI_PAGE] [-t TEXT_CONTENT]
                            [-a ATTACHMENT_PATH] [-c INIFILE]

Create or update MediaWiki pages. Handles content as hypertext, but also page
attachments.

optional arguments:
  -h, --help            show this help message and exit
  -f ACTION, --function ACTION
                        Action to perform: available actions are collect and
                        inject
  -p WIKI_PAGE, --page WIKI_PAGE
                        Wiki page to create, update or retrieve (depends on
                        the requested action)
  -t TEXT_CONTENT, --text TEXT_CONTENT
                        Path to the text file containing the new content of
                        the page
  -a ATTACHMENT_PATH, --attach ATTACHMENT_PATH
                        Path to the file to attach
  -c INIFILE, --configfile INIFILE
                        Configuration parameters provided inside a .ini file.
                        These parameters will be overwritten by inline
                        parameters

```


## Download the contents of a wiki page ##

Choose option *-f collect* in order to download a seleted page as PDF into a file.


```
./mediawiki_manager.py -c config/alozalv.ini -f collect     
Loading pages (1/6)
Counting pages (2/6)                                               
Resolving links (4/6)                                                       
Loading headers and footers (5/6)                                           
Printing pages (6/6)
Done                                                                      
Check the results at /tmp/Angelica_Test.pdf
== DONE! ==

```

Additional info is traced using *logging*. Provide a path of choice in your configuration file to do so.
