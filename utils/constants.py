'''
Media Wiki Manager Constants
'''

LOG_FILE = "log_file"
LOG_FORMAT='[%(asctime)s] %(levelname)s [%(funcName)s:%(lineno)s] %(message)s'
OUTPUT_DIR = "output_dir"

SECTION_GENERAL = "general"
SECTION_WIKI = "wiki"

WIKI_API = "wiki_api"
WIKI_USERNAME = "wiki_username"
WIKI_PASSWORD = "wiki_password"
TEXT_CONTENT = "text_content"
FILE_PATH = "file_path"
WIKI_PAGE = "wiki_page"

ACTION = "action"
ACTION_COLLECT = "collect"
ACTION_INJECT = "inject"


